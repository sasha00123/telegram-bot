import os

import urllib.parse as urlparse
from peewee import *


TOKEN = "531508617:AAH8y504jsOX8LIKbKgQcuwDaCm9aPIncTQ" # get it from BotFather
PORT = int(os.environ.get('PORT', '8443'))
BOT_USERNAME = "Simple_welcome_bot" # It's required when the bot joins the group
PRODUCTION = True # change it to False if you wish to run it on your PC
HEROKU_APP_NAME = "telegram-welcome-bot"

if PRODUCTION:
    urlparse.uses_netloc.append("postgres")
    url = urlparse.urlparse(os.environ["DATABASE_URL"])
    DATABASE = PostgresqlDatabase(autocommit=True, autorollback=True,
        database=url.path[1:],
        user=url.username,
        password=url.password,
        host=url.hostname,
    )
else:
	DATABASE = SqliteDatabase('welcome.db', autocommit=True, autorollback=True)

