This is a Welcome Bot source code.

All the messages are places in messages.py file.
All the configs are in config.py file.

If you deploy to heroku, change your config.py file and upload a zip.
!!! IMPORTANT !!! PostgreSQL add-on has to be enabled.

If you run it on your own PC, change PRODUCTION to False in config.py and run:
	- sh run-dev.sh

If you want to run it on some VPS server, contact the developer.

DATABASES:
	- The Postgresql database is used for production.
	- The Sqlite3 database is used for testing.