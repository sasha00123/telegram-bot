import os
import config
import messages

from telegram.ext import Updater, MessageHandler, Filters, CommandHandler, ConversationHandler
from records import Record


def start(bot, update):
    """
    This function starts when the users writes to the Bot.
    The Bot send some instructions.
    """
    update.message.reply_text(messages.admin_greeting)


def join(bot, update):
    """
    This function starts when someone has invited the bot the group.
    It creates Record instance in the Database and says 'Hello' to everyone.
    """
    try:
        Record.get_or_create(chat_id=update.message.chat.id, message_text=messages.welcome_message)
    except Exception as e:
        update.message.reply_text(str(e))
    update.message.reply_text(messages.about_text)
    
def welcome(bot, update):
    """
    This function sends message to the group from the database when someone
    joins the chat
    """
    update.message.reply_text(
        Record.get(Record.chat_id==update.message.chat.id).message_text.replace(
            "$username",
            update.message.new_chat_members[0].first_name
        )
    ) 

    
def new_member(bot, update):
    """
    When notification about a new member appears in the chat, checks if there 
    are more then one newcomer or the new member is our Bot.
    """
    if len(update.message.new_chat_members) > 1:
        return
    if update.message.new_chat_members[0].username == config.BOT_USERNAME:
    	join(bot, update)
    else:
        welcome(bot, update)
        
        
def is_admin(bot, update):
    """
    Checks if user has administrator rights
    """
    return update.message.chat.get_member(update.message.from_user.id).status in ["administrator", "creator"]


NEW_WELCOME_MESSAGE = 1 # ConversationHandler state


def update_welcome_message(bot, update):
    """
    This function executed when someone types /welcome. It checks for admin
    privileges and the chat type. Asks to enter new welome message if 
    everything goes fine
    """
    if update.message.chat.type != "group":
        update.message.reply_text(messages.not_in_group)
        return
    if not is_admin(bot, update):
        update.message.reply_text(messages.update_message_not_authorized)
        return
    update.message.reply_text(messages.update_message_text)
    return NEW_WELCOME_MESSAGE


def set_message(bot, update):
    """
    This function executes when user sent new welcome message.
    Checks for admin rights, and then update the database record for this chat.
    """
    if not is_admin(bot, update):
        update.message.reply_text(messages.update_message_not_authorized)
        return NEW_WELCOME_MESSAGE
    record = Record.get(Record.chat_id == update.message.chat.id)
    record.message_text = update.message.text
    record.save()
    update.message.reply_text(messages.update_message_successful)
    return ConversationHandler.END
    
    
def cancel_updating_message(bot, update):
    """
    This function is executed when user enters /cancel in NEW_WELCOME_MESSAGE state
    """
    if not is_admin(bot, update):
        update.message.reply_text(messages.update_message_not_authorized)
        return NEW_WELCOME_MESSAGE
    update.message.reply_text(messages.update_rejected)

        
updater = Updater(config.TOKEN)
dp = updater.dispatcher
dp.add_handler(MessageHandler(Filters.status_update.new_chat_members,
                                              new_member))
dp.add_handler(CommandHandler("start", start))
dp.add_handler(ConversationHandler(
	entry_points = [CommandHandler("welcome", update_welcome_message)],
    states = {
        NEW_WELCOME_MESSAGE: [MessageHandler(Filters.text, set_message)]
    },
    fallbacks = [CommandHandler("cancel", cancel_updating_message)]
))


if config.PRODUCTION:
    updater.start_webhook(listen="0.0.0.0",
                          port=config.PORT,
                          url_path=config.TOKEN)
    updater.bot.set_webhook(
        "https://{}.herokuapp.com/{}".format(config.HEROKU_APP_NAME,
                                             config.TOKEN)
    )
else:
	updater.start_polling()
updater.idle()