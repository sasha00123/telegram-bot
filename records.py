import config
from peewee import *

class Record(Model):
    """
    This class is used to store custom message texts
    """
    chat_id = IntegerField(unique=True)
    message_text = TextField()

    class Meta:
        database = config.DATABASE